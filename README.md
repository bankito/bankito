# bankito

Para ejecutar la aplicación se necesita compilar el contenedor

git submodule update --init --recursive

## Como ejecutar

### En desarrollo

```console
foo@bar:~$ docker-compose up
```

### En producción

```console
foo@bar:~$ docker-compose -f docker-compose.yml -f docker-compose.prod.yml up
```